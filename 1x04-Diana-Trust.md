# Diana Bärsen - I have device trust issues

How, why, what?

Security is pretty important

# culture

New hires are (instantly) treated as extremely talanted people, trusted

Default to open internally, default to secrecu externally
- start from the point of what _not_ to share

Why do we care as a security team?

Open, common culture -- prevents a counterculture

Less root = less creativem innovative, can solve problems only some ways?
Could feel untrusted as an employee. Be indiffernt, which leads to demotivation,
disgruntled. Feel untrusted, spied upon... Culture under attack.

Trust, committment, openness: Trust increased involvement. Openness leads to
better context, leads to faster problem solving.

## Friendly ghost

github:Shopify/`ecto_containment_unit`

"Maintaining the Trust Battery"

# Trust team

Buyer, merchant and employee trust

## Perimeter security model

Single point of entry, heavily guarded, outside is dangerous, inside is
implicitly trusted. Nope.

**BeyondCorp.com** Access depends solely on device and user credentials (in that
point in time).

Netflix LISA, Location Independent Security Ahem, something

Dou Beyond. Etc etc.

Access handled on the state of the identity of the user + that of the device.
Are you employed? MFA? Device in latest OS, encrypted, Jamf? Shopify does nmot
have VPNs, all is in Cloud. Kubernetes for orchestration. Office is then
equivalent to a coffee shop. You can deploy an internal app if your machine
has required tethers.

## Okta

Gsuite - okta - Workday

Trust but verify; log and react on anomalies. Slack bot to inform user of
suspicious login/activity.

# Gear

Device inventory, DEP with pre-staging, hoping to full DEP.

## Tiers

Device tiers 1..4, gives conditional access. Tier 1 needs "only" 2fa, T2 =
mfa, latest OS, encrypted. Reading these are **boring!** So make this more
interactive. Use **Hack Days!** Friendly Ghost for device, security, inventory.
Compare with Netflix Stethoscope.

And then automate it. Slackbot says you're encryption fails. Stuff like that.
"70% of your team's devices are encrypted!". Creates friendly copetition.
Gamification!

## Motivation and Gamification

`Masha Sedova` - Gamifying security awareness using behavioural science and
rewards, to transform security from have to to want to!

* The Power of Habit / Charles Duhigg (The Habit Loop - Cue, routine, reward)
  - habits run automatically

# Why should we care?

We hired ppl to do fantastic work. We did not hire them to be experts in
security. Why don't they care? Maybe they're busy. The request came at a
wrong time. Maybe there's a culture of punishment. Or mandates and stuff,
and they get penalised for not doing things.

How passwords don't have to suck? Build _motivation_ , not jsut tools.
LinkedIN Allstar - to Friendly Ghost. Badges, cake! Internal application
Unicorn. Trust coins.

Gamification is faster than reason why, which might follow
