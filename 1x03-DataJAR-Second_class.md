# Who? DataJAR ltd

- James, used Macs since they were beige :)
- Jamesbensomething
- Richard
- Mad, bos, keeps us all in sales

# The Second-Class Citizen

_"A second-class citizen is a person who is systematically discriminated---"_

Apple? Best in class products, works seamlessly (most of the time), platform
for _productivity_ (personal choice), easy to deploy (in comparison)

IBM, Cisco, Deloitte are doing it.

# The Journey

** Vision, Planning, Implementation **

## Vision

* The reason, desire, aspiration? Turn this to reality.
* Employee choice, rather choose a Mac
* Many ppl were living a digital life at home and an alogue life
  at work...
* _"Ths culture at Apple allows me to ..."_
* UI allows users to solve their own problems
  - 5% of Mac users needed support, compared to 40% of Windows users
  - 100 Macs = ~30k dollars in support savings
* Cheaper in the run

## Planning

* Where are you buying the kit from? Stop. Don't buy it from the dark web wit B.
* Why? DEP, VPP...
* Apple Business manager https://beta.business.apple.com/ (looks like school mgr)
* DEP account - MDM / UAMDM (10.13.2 UADMD is a requirement for UAKEL)
* https://goo.gl...uh
* Allow communications out to `17.0.0.0/8` :) (`*.apple.com` because some stuff
  goes through akamai)
* Allow port `5223` out
* `2195-6` out without proxying
* `80` and `443` out to Apple
* AD? NoMAD? Binding?
  - "Because it's what we've always done"
  - AD is _not_ an inventory system, it's an authentication system
* You can use the serial number as unique identifier
  - secomputerid serialnumber
* Management platform? Jamf? Munki? Both?

## Implementation

Discrimination? If you're an Apple guy, maybe you've been business-discriminated.

Educate customers.
- xenophobia, some can't tell the difference between a computer and a screen
- dongles
- Questions, both ways
- You can't do group policies on Macs

Post Implementation
- It'll take some time
- find out why there's resistance

OK, this was mostly a reminder to be kind and understanding
