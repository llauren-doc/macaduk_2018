# Darren Wallace - Shields up, captn?
\@daz_wallace

_"The challenges of bringing modern security ideas to an existing company"_

* Goals
* Plan
* Snags (technical)
* Snags (human)
* Lessons learned
* Looking towards future

# Goals

* Before we start the work, we need to figure out our Goals.
* What are we trying to protect agains?

Baseline:
* Doing a disservice to customers and/or employees?
* Being "hacked"?
* Being sued or fined?
* May 25th...?
* Being infected?

Targets:
* Client devices
  - macs in use can't be wiped :)
* Cloud hosted services
* End users (you know, people!)

# The Plan

We don't want to completely lock down the users, they are adults.

* Enrolment in a management solution
* Full disk encryption
* Local password policies on all devices
  - don't force renewal without a good reason
* Software patch policies, with deadlines

## Cloud hosted services

* Password policies
* MFA
* Use non-shared Accounts
* Changing admin passwords

## Users

* Creation of an IT security policy document
  - what will be enforced by system, what expected by users
* Discussion of password use
* Documentation!
* Rollout

# Snags (technical)

## Configuration profiles

* Lack of precision
  - you'd have to set values in all tabs ...
* Check the video on amsys/profile-enable-filevault-2

## Local password policies

* Policy does not check existing passwords, applied only on password change!
  - fix with `pwpolicy .. -setpolicy newPasswordRequired=1`

## Multi-factor authentication

* Some apps just don't support it
* Sometimes optional, not enforceable by admins
* Sometimes cost more money
* Some are just weird, you get the QR code for the user when you set it as an admin

## Patching

* Inconvenient times

# Snags (users)

## Technical knowledge

* It's easy to forget the Technical level of users;
  - what's Multi-factor, what's enforceable?
* Personal technology - BYOB
  - not everyone has a smartphone, so SMS based something
* Employee leave
  - deadlines for installation .. but will the people be working, sick,
    honeymoon?
* Documentation
  - wasn't read or understood
  - too much Technical language
* Intentional impairing
  - people would just ignore it
  - escalate
* Had the support from management

# Lessons learned (tips and tricks)

The "Eating your own dog food" -concept.
* If you can't do your job with those changes in place, your users probably
  can't either
* Roll out to bosses also

## Documentation

Documentation should be written to the intended audience.

* Screenshots
* Split it up
* Show it to your family (if it's not confidentiail)

## Staggered rollouts

Don't target the whole company at the same time. Test with the most and least
technical persons. Same goes for documentation.

Get appropriate support and approval (like the board of directors).
  - include the risk of change and risk of not changing

Be nice! Some changes will be annoying for your users...
(but don't be too nice, changes need to come anyway)

Be realistic. Don't lock down everything. Do the appropriate. Some users will
go onto Facebook. Rather than saying No, include when it's appropriate to do
so. Talk with people.

Don't go it alone. There's a community of peers out there.

# Loöoking to the future

* Firmware passwords* Unifying authentication systems (SSO, SAML)
* Better data collection
* More Documentation*
* More aggressive Patching
* Spend more time with departments

https://www.moof-it.co.uk/technical/macaduk-2018-sup
