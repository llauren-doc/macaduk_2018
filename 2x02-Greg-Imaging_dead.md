# Greg Neagle - Imaging _is_ dead, now what?

Many suspected hat `apfs` would be the death of imaging, but no. `man asr`

So it's not dead yet, after all

Imaging = block restoring of an image, resulting in a fully working maching.

Is dead? Dying? No longer recommended? Rich Trouton speculated  that apfis
will end imaging, but Apple updated `asr` to create and restore apfis
file systems.

AutoDMG, imgr, Deploy Studio supports `apfs`

But to boot, the Mac needs to have the correct firmware. Otherwise the machine
does not know how to boot from apfs.

Clever admins have managed to extract the firmware and combine it with the image.

Imaging is not dead! or... maybe ...

Apple: Upgrade macOS on a Mac at your institution HT208020 -- Apple does not
recommend or supprot `monolithic` system imaging when upgrading or updating macOS.

BUT this is not may not mean only EFI firmware. How about Touch bar firmware?
Or your new Mac Pro with Face ID, or brainwave ID... Hardware is diverging and
will have separate firmwares. To fix an unbootable iMac Pro, you may have to
flash the firmware using a USB-C cable to another Mac...

**Install instead of image**

# Setup workflows

monolithic imaging ... skip this

## Moduar imaging

* Base installer + updates + apps = images, deploy to machines
* To _maintain_ those machines, use munki etc

## Thin imaging

* OS and the base minimum of software - OS, munki, local admin (bootstrapping)
* Munki installs the rest of the stuff

## No imaging ("installation based workflows")

* Take machine out of the box, install bootstrapping, Maintaining

# Apple tools: System image utility: NetInstall

`/System/Library/CoreServices/Applications`

Creates an `.nbi` folder. Copy this to a NetBoot server.

product id ≠ package identifier

You have to have this to create a package that goes into NetBoot

`productbuild --identifier .com.org.uniqueid`

You can even add this yourself

# Alternatives?

* `createinstallmedia`
* ... with NetInstall? Can you customize it anyway?
* Create a "customised" installer using Netboot Install, then use
  this image for createinstallmedia

* imagr + startosinstall
  - NetInstall style NetBoot
  - Specially crafted external boot volume
* restor from Google

# iMac Pro

- supports Secure Boot, will not boot from external volumes (oob)
- Target disk mode, recovery boot
- `bootstrappr` by Greg Neagle
- from terminal, `hdiutil attach httpL//macbootstrap`, `/Volumes/bootstrapp/run`

Read Eric Gomex' blogs about DEP+MDM
- simplemdm, airwatch, micromdm works

https://groob.io/posts/dep-micromdm-munki/

`tl;dl`

Get an iMac Pro for testing (for 5k)

https://managingosx.wordpress.com/2018/02/20/macad-uk-2018-conference-links
