# Security stuff (numberlock, jr at Slack)

Agenda:
- why _modern management_
- what is vmware Workspace one
- vmware {code}

# Modern management?

* 1/3 of workforce millennials
* Choice: device type, work Location
* Simplicity: Access to work apps, Self service capabilities
* Privacy: Mix of work and personal, control over level or management

MDM becoming required, due to Kernel extensions

The traditional perimeter security model is old, the new perimeter is
identity (device and user identity)

# Workspace One

Evidently, this is Airwatch + Munki + other nice things + app streaming
(like Windows Word "native" over remote)

github:vmware/replay-app-for-tvos
