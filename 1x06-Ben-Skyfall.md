# Life After macOS Server

_In the beginning, there was Nothing. Then came "Rhapsody" (Server 1.0),
Cheetah (Server 10.0), Puma, Jaguar, Panther, Tiger...
On 24-Jan-2018, the World ends._

**DON'T PANIC!** Apple is giving you time to migrate. You just need to formulate
your game plan ... now.

It's time to lose your fear of the `command line`.

Which host OS?
- macOS
- Linux, BSD
- The dark side
- SaaS

## macOS

**Advantages:** Cuddly. Familiar. You know it. It's UNIX inside. Some GUIs.

**Disadvantages:**
- BSD is not Linux, so the command line switches differ (but there is Homebrew).
- Packages aren't all included/available.
- And Homebrew runs everything as the same user; no different user for different
services.
- Updating can be painful
- No real server hardware

## Linux/BSD

**Advantages:**
- Open source (not necessarily free)
- Lots of server tools generally included
- Easy to update (well, easier)
- Lower hardware requirements (so cheaper)
- No licensing costs to run _N_ virtual machines
- Huge user base

**Disadvantages:**
- BSD is not really Linux, so command line switches differ..
- _So_ many flavours of Linux
- Steepish learning curve
- Easy to break, harder to fix

## Windows
**Advantages**
- You may already have it
- Enterprise support (if you've paid for it)
- Some stuff has GUIs for them

**Disadvantages**
- It's definitely not Linux or UNIX. It's witchcraft. You can run `bash` on it :)
- Documentation for anything _unapproved_ is bad
- Pretty much closed source
- Costs money (much more than macOS server)
- It's Windows

# Strategy

* Server name/DNS records
* Service discovery - Bonjour/Avahi
* Which services do you _really_ need?
* What can/can't you migrate? How will you migrate?
* Security, security and security
* Proof of concept
* Test it. Then test it again.

## Calendar and contacts alternatives

* Apple Calendar and Contact server
  - is Open Source
  - is Python, so runs anywhere
  - Is in use at Apple (good sign that it might not go away) ... right now
  - Authors wrote Twisted/Refocus
  - Not quite as simple to install as drag and drop configuration

* RadiCALe
  - another Open source CalDAV + CardDAV server
  - Python 3
  - Easier to set up than Apple CnC server
    - ... but makes more assumptions
  - Migration path unknown

* Fruux sabre/dav (http://sabre.io/)

* SaaS
  - Gsuite, fully cloud based, not free, migration path unknown
  - Exchange/Office 365, _maybe_ with DAVmail (gateway between Exchange and the client)

## NetInstall

- _"Imaging is not quite dead, but it is dying"_ (just pining for the fjords)
- NetBoot can be done with just DHCP and tftp
- BSDpy
- Jamf NetSUS (which refuses to go away)
- imagr (pyfon)

## DHCP

- Unikey to be deprecated from the macOS
  - check out `/etc/bootpd.plist`
- `serveradmin settings dhcp`

- `kea` - evolution of ISC-DHCP
- dnsmasq
- RADIUS
- Windows DHCP server
- Your firewall

## DNS

- dnsmasq (again!)
- BIND (harder to install, security issues at times)
- PowerDNS
- Windows DNS server
- djbdns :D
- Managed DNS

## Mail (mail transport agents)

- Postfix (is more modern than)
- Exim (or was it the other way around)
- Courier (is even newer)
- Sendmail (old, don't use if you can avoid it)
- SaaS: Gsuite, Office...

## Mail (mail delivery agents)

- Courier
- Cyrius
- Dovecot
- SaaS

## Messages

- ejabberd
- mongooseIM
- prosody
- SaaS: Slack, Microsoft Teams, Google Stuff

## VPN

- OpenVPN
- SoftEther VPN
- `tcpcrypt`? (not for production just yet)
- Hardware: routers, Cisco IPsec, Juniper, ...

## Web server

- Apache (ubiquitous, well documented)
  - MAMP, WAMP (like LAMP)
- nginx (performance)
- lighttpd
- hosted

## Wiki

- You can export in various formats (?!)
  - see Charles Edge's `krypted` blog
- Importing is not going to be fun (sorry)
- MediaWiki (php)
- PMwiki (php)
- WXiki (Java)
- MoinMoin (python)

# I still have Questions

- HOW are we going to migrate?
- Certificates?
-
