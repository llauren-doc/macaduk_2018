# Henry Stammerjohan - Building your macOS baseline requirements (again)

## Today

* Variety of endpoints inside and outside
* Mission to secure hw/sw configuration
* Continuous vulnerability assessment

## Imagine

* You're asked to apply a Windows security guideline to Macs
* So how do GPOs apply to Macs? Why open 17/8?

## Security baseline

Building it is an ongoing challenge w many components

* Basic (security) plan for all IT systems
* Identify and implement security measures
* Complete for operational envoronment
* Specific implementation documents

## Objectives

* Enforce compliance Standards (for stability, relilience, confidence, quality)
* Appropriate strategy to addess security and end user productivity
* Include (smiple) post-incident templates
* This is your security posture

## Procedures

* Frequent patching
* Enable only required services, limit Access, like client to client communications
* Ensure settings stay compliant

Creating policies too rigid will make yours fail.

Check CERN or SANS baselines, or CISecurity.org/benchmark/apple_os

github.com/drduh/macOS-Security-and-Privacy-Guide

Apple is not providing this.

## Configuration elements

* Config profiles (mdm, manually deployed, munki)
* Scripts, cli tools, software
* Conditionals, extension attributes,
* MDM commands (wipe/lock)

## Control facilities

* Inventory ingormation, management system
* Scheduled intervals
* Reporting, dashboards, logging
* _Change detection_, alerting
* Automation, programmed remediation

https://github.com/kristovatlas/osx-config-check

Acknowledge the risk of executing binaries and scripts.

## What can go wrong?

Much. Check the ccc talk.

## Tools?

* Nessus - tenable.com
* Chef Inspec (desired state check), _"compliance as code"_
* Homebrew (and harden with osquery), problematic with change management

## Application lifecycle and change management

* Inspect content
* Test system install, fingerprinting, check what changed within versions...
  - inspect: Java installer includes bash (?!)

## Security baseline to Mamangement infrastructure

* Run it
* Build multiple layers of defense
* Limit accss/API
* What if the management server is down?
* Log aggregation
* http://dev-sec.io - puppet, chef, ansible for security

## osquery, Santa, zentral :)

## Event streams

* Event - point of instrumentation in the system --
* Ship - aggegate resylts and sync configuration
* Probe - filter when certain event happens
* Action - execute once the probe fires

Event stream data is stored for historic inspection

Google Fleet management at scale

# Rebuild your security baseline

https://github.com/apfelwerk/macadUK2018-baseline-requirements

Also have a look at Open BSM Audit and ship to Zentral (and look inside Kibana).
Open BSM will make a _lot_ of data, so you want to audit only a select few
computers...
