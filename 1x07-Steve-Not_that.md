# Stephen - You're not installing _that_ on my laptop

_a tale of MDM, privacy..._

Insight: ThoughtWorks is quite a bit like Reaktor, but ten years older.

Like BYOD, but TW buy the hardware

TW hold techops to Very High Standards. It's a bit like cat herding.
"Because i said so" is not a good enough reason.

Growth of the Mac-
 - Windows machines were managed, Macs were largely ignored
 - from 2013 on, the Mac became the default computer
 - more and more to cloud stuff

Scale?
 - New countries get new offices, startup buzz, localised
 - Local ways of doing things, local cultures, local employees...

Can we do this better? Back in 2015, we decided we could need some help.
Single pane of glass for Windows + Mac... didn't work. JAMF seemed
the best one -- but "JAMF-GATE". Not enough transparency for the users to
accept it. Sent out an email, people were not very cool with that.

Closed source "root level" agents were at best undesirable, at worst
_a complete betrayal of company culture_ ...

OKTA Mobility Management (OMM), building an MDM tool.
 - Licensing per user (not device)
 - There's _no agent/binary_ with OMM

But
 - Remote wipe -- you can do _what_ with my laptop?
 - Dr. Ian Malcolm: _"You can, but should you?"_

Enter: GAMERA. Infosec loved naming the tool.
 - read only OMM view for the users
 - build an API, which quite didn't happen
 - and then High Sierra happened

All of a sudden, an MDM wasn't nice to have, it was must to have.
Same with DEP. And UKULELE. But OMM does not support. So bye bye OKTA.
And WHAT do we do now? Zoom for rooms?

**PANIC** or ask the nice members of the London macadmin slack channel.
(@squirke)

Hello SimpleMDM. Even got Remote Wipe out of it.

What did we learn? It is hard to keep smart opinionated users and admins happy.
People fear the unknown. Agentless = less vendor lock-in, so moving to SimpleMDM
was comparatively painless. And make sure everything works and exists 1.5 weeks
before the conf :)
