# We need to talk (about Deployment)

We invent. We invent stuff. We invent ways. We invent community. We invent
terms.

_"Thin imaging"_

Imaging is possible only to re-install the software to the exact same version
(point version) as currently installed. Image may contain firmware stuff that
is not compatible with an upgrade :/

Workflow Questions: What is your Workflow for
- deploying a new Mac
- repurposing
- phasing out

Workflow types:
- retail/consumer
- lab?
- etc...
