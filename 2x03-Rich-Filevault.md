# FileVault

## Institutional recovery keys

* You can rotate the password of the keychain without changing the IRK
* IRK contains a Certificate to decrypt

Here's how

```
sudo security create-filevaultmaster-keychain /Library/Keychains/FileVaultMaster.keychain
cp /Library/Keychains/FileVaultMaster.keychain /Volmes/your_safe_backup
open /Library/Keychains/FileVaultMaster.keychain
% remove private key from above keychain
```

## Deploying IRKs

* Install with installer Packages
* Include with your image
* Copy it to your Macs using system mgmt tools
* A `FileVaultMaster.keychain` _can_ be deployed to any number of Macs

* System Preferences
* Security and privacy
* FileVault - Turn on - discovers IRK automatically

## Common Management

`fdesetup`

`fdesetup enable -inputplist` Note: all account passwords in clear text :(

Or even better, `-defer /path/to/recovery.plist` asks for password on next logout

You can use `-defer` only once and only for one user.

`fdesetup enable -forceatlogin <n>` where n = 0 or more

Use both IRK and PRK, `fdesetup enable -keychain`. Only IRK, use
`-norecoverykey` (= no _personal_ recovery key)

`# fdesetup authrestart -delayinminutes <n>` will reboot without asking for
FileVault password when the machine comes back up. (n = -1 waits idefinitely).
You could use this on your encrypted engine room Mac after booting it :)

## And then there are GUI tools

You can decrypt your FileVault using the `Reset password` at boot-up, telling
it that you can't type the password :O

# Divergence APFS vs HFS+

`diskutil apfs | corestorage`

HFS: `diskutil cs list`, get logical volume UUID and decrypt
APFS: `diskutil apfs list`, `diskutil apfs unlockVolume /dev/disk1s1`

`diskutil apfs listcryptousers </dev/disk1s1>`

## APFS Secure token

`sysadminctl interactive -secureTokenStatus <username>`

I don't really understand what the Secure Token really is, but it sounds
relevant to HighC
