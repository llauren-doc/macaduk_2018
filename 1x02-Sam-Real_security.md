# Sam Keeley - Real (?) security

Last year ended with "Part 2, the important one", which is where this year's
pres begins :)

## Recap, part 1

* Reduce risk - _"The pursuit of security should really be
  the pursuit of risk reduction"_
* Update, always
* Encrypt everything
* Embrace new (security) features (secure token...)
* Audit what truly matters
* Local admin rights are :thumbs_up: now

```
Security TL;DR:
Make yourself expensive to attack so that they just target someone else.
```

Good security practices should be part of our mindset (Zen)

# What's a Mac admin's role these days anyway?

* *Enabler:* Provides an org and its users mechanisms for effective use of Apple
  technology
* *Protector:* Accounts for and reduces risk commonly associated with consumer
  and enterprise technologies
* *Balancer:* Finds and implements the right _balance_ of enablement and
  protection, optimally propelling both forwards.

What if we don't do all these things?

# Risks of compromise

* Operational loss (MacKeeper or other malware clogs up your machine/operation)
* Financial loss (compromising financial systems)
* Organisational data loss (losing internal data, like HR)
* User data loss

# What to defend against

* Common attacks, not specifically targeted at you
  - spyware, generic phishing...
  - things you should be able to common with an antivirus tool
  - there _is_ real Mac malware out there
* Targeted attacks
  - informed attackers focuses on known deficiencies
  - unlikely to be detectable by common tooling; _They_ might even know what
    protection mechanisms you have employed
  - Increase cost of attacks by shrinking window of risk

# Prioritise the big wins (lower hanging fruit)

Postpone the extreme edge cases, focus on the obvious
* You have 1000 macs, so encrypt them all

Attackers target the admins. You are \#1 so protect yourself

# How do i get pwnd?

What not to do

* Scripts: What are they really doing?
  - `curl | bash` is not necessarily a Good Thing
  - what are the _last_ things they change on your system?
  - what are the differences on your machine before and after they run?
* Packages: Should i be testing this here?
  - Maybe not on your admin box?
* Privileged actions: What access did i just grant?
  - Your access as a sysadmin is very broad ...
  - Phishing, or legit, but Hi Jake, Google Docs would like to
    - read, send, delete and manage your email
    - manage your contacts
  - Clever fake google doc share, using actual Google login (!)
    - will re-email itself using those contacts, so it's a
      classic virus, using oauth
    - Now consider this as a targeted attack

  If someone can send email as you, how far could they get? Very.

  ## Defending yourself as a mac admin

  * We are all vulnerable at some level
  * If you're compromised, so is everyone else in our organisation
  * Never exempt yourself (if anything, _you_ should have more policies than users)
    - antivirus
    - mfa
    - security policies
  * At some point, we've all got a little sloppy with anaudited scripts or apps

# Your own risks

* A Mac admin touches a lot of systems in many environments, which can make it hard to keep everything in line
* Pivoting through the mac admin is simple, as our tooling is generally easy to use and exposed
* A dedicated attacker can easily target one or many users
* Would your organisation detect malicious use/change of management tools?

# Practice makes per.. make better

Be the master of your domain; propel good security
Make the Mac look like the secure platform that it is ... so you don't have to use Windows

# Passwords

* What really _happens_ when submitting a password?
* Refocus on passwords. Use unique passwords per account and service.
  - use a password manager
  - if every password is unique, periodic password changes are less useful
* When sending a password to an application, assume they're doing their worst
  with it...
  - Salting and hashing is hard :)
* SSH keys
  - the compute required to crack a 2048 bit sha256 encrypted key is
    currently millennia (probably more)
  - easy to put multiple public keys on a server, impossible with passwords
  - can be encrypted, using a local password (not sent to remote)
* Smart cards
  - get yubikey (or something) for your ssh keys
* MFA
  - set up mfa on everything, _including_ your personal Accounts
  - keeps an attacker from being you
  - avoid phone/sms auth

  Keep your users safe by keeping yourself safe

# Reassess your approach
